
syntax match r /r/
highlight r cterm=bold ctermfg=red
syntax match s /s/
highlight s ctermfg=red

syntax match g /g/
highlight g cterm=bold ctermfg=green
syntax match h /h/
highlight h ctermfg=green

syntax match o /o/
highlight o cterm=bold ctermfg=yellow
syntax match p /p/
highlight p ctermfg=yellow

syntax match u /u/
highlight u cterm=bold ctermfg=blue
syntax match v /v/
highlight v ctermfg=blue

syntax match m /m/
highlight m cterm=bold ctermfg=magenta
syntax match n /n/
highlight n ctermfg=magenta

syntax match y /y/
highlight y cterm=bold ctermfg=cyan
syntax match z /z/
highlight z ctermfg=cyan

syntax match e /e/
highlight e cterm=bold ctermfg=grey
syntax match f /f/
highlight f ctermfg=white

syntax match a /a/
highlight a cterm=bold ctermfg=black
syntax match b /b/
highlight b ctermfg=black

syntax match t /t/
highlight t ctermfg=grey

